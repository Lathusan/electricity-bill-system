package com.demo;

import com.allClass.CalculateElectricityBill;
import com.allClass.Option;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * @Mar 19, 2021
 **/

public class Demo {

	public static void main(String[] args) {

		System.out.println(" Welcome to CEB");
		System.out.println("================\n");
		System.out.println("1. Add new account");
		System.out.println("2. View account");
		System.out.println("3. Bill Calculate");
		System.out.println("4. Exit");
		
		Option option = new Option();
		option.optionMethod();

	}

}
