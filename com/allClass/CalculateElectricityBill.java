package com.allClass;

import java.util.Scanner;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * @Mar 19, 2021
 **/

public class CalculateElectricityBill {

	double billpay;

	long units;

	public void CalculateBill() {

		Scanner scan = new Scanner(System.in);

		System.out.print("Enter the number of units : ");

		units = scan.nextLong();

		if (units < 90)
			billpay = units * 10;

		else if (units <= 90 && units <= 120)
			billpay = units * 15;

		else if (units > 120)
			billpay = units * 20;

		System.out.println("Bill to pay : " + billpay + "\n");
	}

}
