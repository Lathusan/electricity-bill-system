package com.allClass;

/**
* @author  Lathusan Thurairajah
* @email   lathusanthurairajah@gmail.com
* @version 1.0
* @Mar 20, 2021
**/

import java.sql.*;
import java.io.*;

public class Sample {
	
	public static void main(String args[]) throws Exception {
		
		System.out.println("MySQL Connect Example.");
		Connection conn = null;
		String url = "jdbc:mysql://localhost:3306/";
		String dbName = "ecb";
		String driver = "com.mysql.jdbc.Driver";
		String userName = "root";
		String password = "root";
		Class.forName(driver).newInstance();
		conn = DriverManager.getConnection(url + dbName, userName, password);
		System.out.println("Connected to the database");
		String City, Name;
		int MobNo, addAccNo;
		PreparedStatement ps = conn.prepareStatement("insert into addaccount values(?,?,?,?)");
		Statement stmt = conn.createStatement();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		do {
//int id=Integer.parseInt(br.readLine());
//float salary=Float.parseFloat(br.readLine());
//ps.setFloat(2, salary);
			System.out.println("enter mobno:");
			MobNo = br.read();
			System.out.println("enter name:");
			Name = br.readLine();
			System.out.println("enter city:");
			City = br.readLine();
			System.out.println("enter accountNo:");
			addAccNo = br.read();
			ps.setString(2, Name);
			ps.setString(3, City);
			ps.setInt(1, MobNo);
			ps.setInt(4, addAccNo);
			int i = ps.executeUpdate();
			System.out.println(i + " records added");
			System.out.println("Do you want to continue: y/n");
			String s = br.readLine();
		
			if (s.startsWith("n")) {
				break;
			}
		} while (true);
		String sql = "SELECT * from sq";
		ResultSet rs = stmt.executeQuery(sql);
		System.out.println("The records are :");
		while (rs.next()) {
			City = rs.getString(3);
			Name = rs.getString(2);
			MobNo = rs.getInt(1);
			addAccNo = rs.getInt(4);
			
			System.out.println(rs.getRow() + "-" + Name + " " + City + " " + MobNo + " " + addAccNo);
		} // end while
		conn.close();
	}
}
