package com.allClass;

import java.util.Scanner;

import com.demo.Demo;

/**
 * @author  Lathusan Thurairajah
 * @email   lathusanthurairajah@gmail.com
 * @version 1.0
 * @Mar 19, 2021
 **/

public class ConClass {
	
	Scanner scan = new Scanner(System.in);
	Demo demo = new Demo ();
	
	void con () {
		System.out.println("If you want to continue?\n 1. Yes\n 2. No");
		int yorn = scan.nextInt();
		
		if ( yorn == 1) {
			System.out.println();
			demo.main(null);
		}else {
			System.out.println("\nGood Bye !!!");
			System.exit(0);
		}
	}

}
