package com.allClass;

import java.util.Scanner;
import java.sql.*;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * @Mar 19, 2021
 **/

public class EcbAccount {
	Sample sample = new Sample();

	int arrCon = 0;

	String name[] = new String[10];
	String city[] = new String[10];
	int mobNo[] = new int[10];
	String accNo[] = new String[10];

	ConClass conClass = new ConClass();

	void addAccount() {
		Scanner scan = new Scanner(System.in);

		System.out.print("\nWelcome to the new user. Please enter your \nName : ");
		name[arrCon] = scan.next();

		System.out.print("City : ");
		city[arrCon] = scan.next();

		System.out.print("MobNo : ");
		mobNo[arrCon] = scan.nextInt();

		System.out.print("Account No : ");
		accNo[arrCon] = scan.next();

		arrCon++;

		System.out.println("Thanks for your info. Your account has been created.\n");

		for (int i = 0; i < 10; i++) {
			System.out.println(accNo[i] + "\t" + name[i] + "\t" + city[i] + "\t" + mobNo[i]);
		}
		conClass.con();

	}

	void viewAcc() {

		System.out.println("\nAcc No\tName\tCity\tMob No");
		System.out.println("======\t====\t====\t======");

		for (int i = 0; i < 10; i++) {
			System.out.println(accNo[i] + "\t" + name[i] + "\t" + city[i] + "\t" + mobNo[i]);
		}

		System.out.println();
		conClass.con();
	}

}
