package com.allClass;

import java.util.Scanner;

import com.demo.Demo;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * @Mar 19, 2021
 **/

public class Option {

	public static void optionMethod() {

		Scanner scan = new Scanner(System.in);
		ConClass conClass = new ConClass();

		System.out.print("Please select one : ");
		int option = scan.nextInt();
		EcbAccount account = new EcbAccount();
		
		switch (option) {
		case 1:
			account.addAccount();
			conClass.con();

		case 2:
			account.viewAcc();
			conClass.con();
			
		case 3:
			CalculateElectricityBill bill = new CalculateElectricityBill();
			bill.CalculateBill();
			conClass.con();

		case 4:
			System.out.println("\nGood Bye !!!");
			break;

		default:
			System.err.println("Please select correct input !!!\n");
			Demo demo = new Demo();
			demo.main(null);
		}

	}

}
